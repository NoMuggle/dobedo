from prototypeinterface import Prototype
import argparse


def registration(args):
    try:
        Prototype.registration(args.login, args.password)
        print('Registered successfully')
    except ValueError as e:
        print(e)


def login(args):
    try:
        Prototype.authorize(args.login, args.password)
        print('Authorized successfully')
    except ValueError as e:
        print(e)


def print_all_users():
    for user in Prototype.users:
        print(Prototype.users[user])


def print_user():
    print(Prototype.cur_user)


def print_tasks(args):
    mode = 'current'
    if args.completed:
        mode = 'completed'
    elif args.failed:
        mode = 'failed'
    try:
        for task in Prototype.get_task_list(mode):
            print(task)
    except TypeError as e:
        print(e)


def inspect_task(args):
    print(Prototype.get_full_task_info(args.id))


def inspect_board_task(args):
    print(Prototype.get_full_board_task_info(args.id, args.board_id))


def add_task(args):
    try:
        Prototype.add_task(args.name, args.description, args.tags, args.priority, args.parent, args.end, args.period,
                           args.notify)
        print('Added successfully')
    except TypeError as e:
        print(e)


def complete_task(args):
    try:
        Prototype.complete_task(args.id)
        print('task id: ' + args.id + ' completed')
    except Exception as e:
        print(e)


def sort_tasks(args):
    mode = ''
    if args.name:
        mode = 'name'
    elif args.timer:
        mode = 'timer'
    elif args.priority:
        mode = 'priority'
    try:
        Prototype.sort_user_tasks(mode)
    except Exception as e:
        print(e)


def edit_task(args):
    try:
        Prototype.edit_task(task_id=args.id, name=args.name, description=args.description, tags=args.tags,
                            priority=args.priority, timer=args.end, period=args.period)
        print('Task id: '+args.id+' edited')
    except Exception as e:
        print(e)


def move_task(args):
    try:
        Prototype.move_task(args.source, args.destination)
        print('Moved successfully')
    except Exception as e:
        print(e)


def remove_task(args):
    try:
        Prototype.remove_task(args.id)
        print('Removed successfully')
    except Exception as e:
        print(e)


def open_board(args):
    try:
        Prototype.load_board(args.board_id)
        print('Board loaded')
    except Exception as e:
        print(e)


def add_board(args):
    try:
        Prototype.new_board(args.name)
        print('New Board `' + args.name + '` was created')
    except Exception as e:
        print(e)


def print_projects():
    for project in Prototype.get_boards():
        print(project)
        print('-'*20)


def add_task_to_board(args):
    try:
        Prototype.add_board_task(args.name, args.description, args.tags, args.parent, end=args.end,
                                 priority=args.priority, board_id=args.board_id, period=args.period)
        print('Added successfully')
    except Exception as e:
        print(e)


def edit_board(args):
    try:
        Prototype.edit_board_task(args.id, args.name, args.description, args.tags, end=args.end,
                                  board_id=args.board_id, priority=args.priority, period=args.period)
        print('Edited successfully')
    except Exception as e:
        print(e)


def print_board_tasks(args):
    mode = 'current'
    if args.completed:
        mode = 'completed'
    elif args.failed:
        mode = 'failed'
    try:
        for task in Prototype.get_board_task_list(mode, args.board_id):
            print(task)
    except Exception as e:
        print(e)


def complete_board_task(args):
    try:
        Prototype.complete_board_task(args.task_id, args.board_id)
        print('Completed successfully')
    except Exception as e:
        print(e)


def print_board_users(args):
    for user in Prototype.get_board_users(args.board_id):
        print(user)


def remove_board_task(args):
    try:
        Prototype.remove_board_task(args.task_id, args.board_id)
        print('Removed successfully')
    except Exception as e:
        print(e)


def move_board_task(args):
    try:
        Prototype.move_board_task(args.source, args.destination, args.board_id)
        print('Moved successfully')
    except Exception as e:
        print(e)


def sort_board_tasks(args):
    mode = ''
    if args.name:
        mode = 'name'
    elif args.timer:
        mode = 'timer'
    elif args.priority:
        mode = 'priority'
    try:
        Prototype.sort_board_tasks(mode)
    except Exception as e:
        print(e)


def parse_args():
    parser = argparse.ArgumentParser()

    subparsers = parser.add_subparsers(help='sub-command help')

    users_parser = subparsers.add_parser('user', help='Log in or watch users')

    user_subparsers = users_parser.add_subparsers(help='sub-command help')

    login_parser = user_subparsers.add_parser('log', help='Log in the system')
    login_parser.add_argument('-l', '--login', help='Your login', default='user')
    login_parser.add_argument('-p', '--password', help='Your password', default='123')
    login_parser.set_defaults(func=login)

    register_parser = user_subparsers.add_parser('reg', help='Register in the system')
    register_parser.add_argument('-l', '--login', help='Your login', default='user', required=True)
    register_parser.add_argument('-p', '--password', help='Your password', default='123', required=True)
    register_parser.set_defaults(func=registration)

    list_parser = user_subparsers.add_parser('all', help='List of all users')
    list_parser.set_defaults(func=print_all_users)
    who_parser = user_subparsers.add_parser('whoami', help='Display logged user')
    who_parser.set_defaults(func=print_user)

    task_parser = subparsers.add_parser('task', help='Create or manage tasks')

    task_subparsers = task_parser.add_subparsers(help='sub-command help')

    add_parser = task_subparsers.add_parser('add', help='Add task to your task list')
    add_parser.add_argument('-n', '--name', help='Task name', default='Simple task', required=True)
    add_parser.add_argument('-d', '--description', help='Task description', default='be a good man', required=False)
    add_parser.add_argument('-t', '--tags', help='Task tags', nargs='+', required=False)
    add_parser.add_argument('-r', '--priority', help='Task priority (0-4). 0 - highest priority', default=0)
    add_parser.add_argument('-p', '--parent', help='ID of parent task', default=0)
    add_parser.add_argument('-e', '--end', help='Task completion time in format [DD.MM.YYYY HH:MM]',
                            default='12.12.2020 00:00')
    add_parser.add_argument('-pe', '--period',
                            help='Period of repeating in format d - day; w - week; m - month; y - year', default=None)
    add_parser.add_argument('-tn', '--notify', help='notifications', default='')
    add_parser.set_defaults(func=add_task)

    task_list_parser = task_subparsers.add_parser('list', help='Print all your tasks')
    print_group = task_list_parser.add_mutually_exclusive_group()
    print_group.add_argument('-p', '--pending', action='store_true', help='Print all pending tasks')
    print_group.add_argument('-c', '--completed', action='store_true', help='Print all completed tasks')
    print_group.add_argument('-f', '--failed', action='store_true', help='Print all failed tasks')
    task_list_parser.set_defaults(func=print_tasks)

    task_sort_parser = task_subparsers.add_parser('sort', help='Sort tasks in specific order')
    sort_group = task_sort_parser.add_mutually_exclusive_group()
    sort_group.add_argument('-n', '--name', action='store_true', help='Sort tasks by name')
    sort_group.add_argument('-t', '--timer', action='store_true', help='Sort tasks by deadline')
    sort_group.add_argument('-p', '--priority', action='store_true', help='Sort tasks by priority')
    task_sort_parser.set_defaults(func=sort_tasks)

    complete_parser = task_subparsers.add_parser('complete', help='Complete task ID')
    complete_parser.add_argument('id', help='Id of completed task')
    complete_parser.set_defaults(func=complete_task)

    edit_parser = task_subparsers.add_parser('edit', help='Edit task with selected id')
    edit_parser.add_argument('-i', '--id', help='Task id', default=0)
    edit_parser.add_argument('-n', '--name', help='New task name', default=None)
    edit_parser.add_argument('-d', '--description', help='New task description', default=None)
    edit_parser.add_argument('-t', '--tags', help='New task tags', nargs='+', default=None)
    edit_parser.add_argument('-r', '--priority', type=int,
                             help='New task priority (0-9). 0 - highest priority', default=None)
    edit_parser.add_argument('-e', '--end',
                             help='New deadline of deadline in format [DD.MM.YYYY HH:MM]', default=None)
    edit_parser.add_argument('-pe', '--period', default=None,
                             help='New period of repeating in format d - day; w - week; m - month; y - year')
    edit_parser.set_defaults(func=edit_task)

    move_parser = task_subparsers.add_parser('move', help='Move task #source to sub tasks of task #destination')
    move_parser.add_argument('-s', '--source', help='Id of parent task', required=True)
    move_parser.add_argument('-d', '--destination', help='Id of task you want to move', required=True)
    move_parser.set_defaults(func=move_task)

    remove_parser = task_subparsers.add_parser('remove', help='Remove task #ID')
    remove_parser.add_argument('id', help='Id of task to remove')
    remove_parser.set_defaults(func=remove_task)

    more_parser = task_subparsers.add_parser('more', help='Show full information of task #ID')
    more_parser.add_argument('id', help='Id of task to inspect')
    more_parser.set_defaults(func=inspect_task)

    boards_parser = subparsers.add_parser('board', help='Manage boards')

    board_subparsers = boards_parser.add_subparsers(help='sub-command help')

    board_add_parser = board_subparsers.add_parser('open', help='Open new board')
    board_add_parser.add_argument('board_id', help='Board id')
    board_add_parser.set_defaults(func=open_board)

    board_open_parser = board_subparsers.add_parser('create', help='Create new board')
    board_open_parser.add_argument('-n', '--name', help='Board name', default='Simple board', required=True)
    board_open_parser.set_defaults(func=add_board)

    board_list_parser = board_subparsers.add_parser('list', help='Print all boards')
    board_list_parser.set_defaults(func=print_projects)

    board_add_parser = board_subparsers.add_parser('add', help='Add task on the board')
    board_add_parser.add_argument('-b', '--board_id', help='Board id', default=0)
    board_add_parser.add_argument('-n', '--name', help='Task name', default='Simple task', required=True)
    board_add_parser.add_argument('-d', '--description', help='Task description', default='', required=False)
    board_add_parser.add_argument('-t', '--tags', help='Task tags', nargs='+', required=False)
    board_add_parser.add_argument('-r', '--priority', type=int,
                                  help='Task priority (0-4). 0 - highest priority', default=0)
    board_add_parser.add_argument('-p', '--parent', help='ID of parent task', default=0)
    board_add_parser.add_argument('-e', '--end',
                                  help='Task completion time in format [DD.MM.YYYY HH:MM]', default=None)
    board_add_parser.add_argument('-pe', '--period', default=None,
                                  help='Period of repeating in format d - day; w - week; m - month; y - year')
    board_add_parser.set_defaults(func=add_task_to_board)

    board_edit_parser = board_subparsers.add_parser('edit', help='Edit task with known id')
    board_edit_parser.add_argument('-b', '--board_id', help='Board id', default=0)
    board_edit_parser.add_argument('-i', '--id', help='Task id', default=0)
    board_edit_parser.add_argument('-n', '--name', help='New task name', default=None)
    board_edit_parser.add_argument('-d', '--description', help='New task description', default=None)
    board_edit_parser.add_argument('-t', '--tags', help='New task tags', nargs='+', default=None)
    board_edit_parser.add_argument('-r', '--priority', type=int,
                                   help='New task priority (0-4). 0 - highest priority', default=None)
    board_edit_parser.add_argument('-e', '--end',
                                   help='Task completion time in format [DD.MM.YYYY HH:MM]', default=None)
    board_edit_parser.add_argument('-pe', '--period',
                                   help='New period of repeating in format d - day; w - week; m - month; y - year',
                                   default=None)
    board_edit_parser.set_defaults(func=edit_board)

    board_task_list_parser = board_subparsers.add_parser('list_tasks', help='Print tasks in board id: ')
    board_task_list_parser.add_argument('-b', '--board_id', help='Board id', default=0)
    project_print_group = board_task_list_parser.add_mutually_exclusive_group()
    project_print_group.add_argument('-p', '--pending', action='store_true', help='Print all pending tasks')
    project_print_group.add_argument('-c', '--completed', action='store_true', help='Print all completed tasks')
    project_print_group.add_argument('-f', '--failed', action='store_true', help='Print all failed tasks')
    board_task_list_parser.set_defaults(func=print_board_tasks)

    board_complete_parser = board_subparsers.add_parser('complete', help='Complete board task ID')
    board_complete_parser.add_argument('-b', '--board_id', help='Board id', default=0)
    board_complete_parser.add_argument('task_id', help='Id of completed task')
    board_complete_parser.set_defaults(func=complete_board_task)

    board_remove_parser = board_subparsers.add_parser('remove', help='Remove task from board')
    board_remove_parser.add_argument('-b', '--board_id', help='Board id', default=0)
    board_remove_parser.add_argument('task_id', help='Id of task to remove')
    board_remove_parser.set_defaults(func=remove_board_task)

    board_move_parser = board_subparsers.add_parser('move', help='Move board task #source to sub tasks of task '
                                                    '#destination')
    board_move_parser.add_argument('-b', '--board_id', help='Board id', default=0)
    board_move_parser.add_argument('-s', '--source', help='Id of parent task', required=True)
    board_move_parser.add_argument('-d', '--destination', help='Id of task you want to move', required=True)
    board_move_parser.set_defaults(func=move_board_task)

    board_more_parser = board_subparsers.add_parser('more', help='Show full information about task #ID')
    board_more_parser.add_argument('id', help='Id of task to inspect')
    board_more_parser.add_argument('-b', '--board_id', help='Board id', default=0)
    board_more_parser.set_defaults(func=inspect_board_task)

    board_users_parser = board_subparsers.add_parser('users', help='Print all users on board')
    board_users_parser.add_argument('-b', '--board_id', help='Board id', default=0)
    board_users_parser.set_defaults(func=print_board_users)

    board_task_sort_parser = board_subparsers.add_parser('sort', help='Sort board tasks in specific order')
    board_sort_group = board_task_sort_parser.add_mutually_exclusive_group()
    board_sort_group.add_argument('-n', '--name', action='store_true', help='Sort tasks by name')
    board_sort_group.add_argument('-t', '--timer', action='store_true', help='Sort tasks by deadline')
    board_sort_group.add_argument('-p', '--priority', action='store_true', help='Sort tasks by priority')
    board_task_sort_parser.set_defaults(func=sort_board_tasks)

    args = parser.parse_args()
    if 'func' in args:
        args.func(args)


def main():
    parse_args()
    Prototype.save_users()
    Prototype.save_project()


if __name__ == '__main__':
    Prototype = Prototype()
    main()