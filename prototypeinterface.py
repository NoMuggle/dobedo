from components import User, Board
from components.TaskBoard import TaskBoard
from components.Task import *
import os
import jsonpickle
import pathlib


class Prototype:
    def __init__(self, config_path=os.path.join('..', '..', 'config', 'config.ini'), database_folder=None,
                 users_file=None, cur_user_file=None, boards_folder=None):
        self.users = {}
        self.board = None
        self.cur_user = None
        config = parse_config(config_path)
        default = None
        if config:
            default = config['DEFAULT']
        if database_folder:
            self.database_folder = database_folder
        elif default:
            self.database_folder = default['database_folder']
        else:
            self.database_folder = 'data'

        if users_file:
            self.users_file = users_file
        elif default:
            self.users_file = default['users_file']
        else:
            self.users_file = 'users.json'

        if cur_user_file:
            self.cur_user_file = cur_user_file
        elif default:
            self.cur_user_file = default['cur_user_file']
        else:
            self.cur_user_file = 'cur_user.txt'

        if boards_folder:
            self.boards_folder = boards_folder
        elif default:
            self.boards_folder = default['boards_folder']
        else:
            self.boards_folder = 'boards'
        try:
            self.load_users()
            self.load_board('last_board')
        except FileNotFoundError:
            pass
        finally:
            if self.cur_user:
                self.cur_user.update_tasks()

    def load_current_user(self):
        if not os.path.exists(os.path.join(self.database_folder, self.cur_user_file)):
            open(os.path.join(self.database_folder, self.cur_user_file), 'w').close()
            self.cur_user = None
        else:
            with open(os.path.join(self.database_folder, self.cur_user_file), 'r', encoding='utf-8') as f:
                try:
                    login = f.readline().strip()
                    self.cur_user = self.users[login]
                except KeyError:
                    self.cur_user = None

    def load_users(self):
        if not os.path.exists(self.database_folder):
            os.mkdir(self.database_folder)
            self.users = None
        elif os.path.exists(os.path.join(self.database_folder, self.users_file)):
            with open(os.path.join(self.database_folder, self.users_file), 'r') as f:
                self.users = jsonpickle.decode(f.readline())
        self.load_current_user()

    def registration(self, login, password):
        if self.users.get(login, None):
            raise ValueError('User with this login already exists')
        new_user = User.User(login, password)
        self.users[login] = new_user
        self.cur_user = new_user

    def authorize(self, login, password):
        if login in self.users:
            if self.users[login].check_password(password):
                self.cur_user = self.users[login]
            else:
                raise ValueError('Wrong password')
        else:
            raise ValueError('User does not exist')

    def save_users(self):
        if not os.path.exists(self.database_folder):
            os.mkdir(self.database_folder)
        with open(os.path.join(self.database_folder, self.users_file), 'w+') as f:
            f.write(jsonpickle.encode(self.users))
        with open(os.path.join(self.database_folder, self.cur_user_file), 'w', encoding='utf-8') as f:
            if self.cur_user:
                f.write(self.cur_user.name)

    def add_task(self, name, description, tags, priority=Priority.small, dependence=0, timer=None, period=None,
                 types_notifications=''):
        if not self.cur_user:
            raise AttributeError('You are not logged in')
        task = Task(name=name, tags=tags, description=description, dependence=dependence,
                    priority=priority, period=period, end=timer,
                    types_notification=types_notifications)
        self.cur_user.add_task(task)

    def complete_task(self, task_id):
        if not self.cur_user:
            raise AttributeError('U R not logged in')
        self.cur_user.complete_task(task_id)

    def edit_task(self, task_id, name, description, tags, priority=Priority.small, timer=None, period=None):
        if self.cur_user is None:
            raise AttributeError('U R not logged in')
        self.cur_user.edit_task(task_id=task_id, name=name, description=description, tags=tags,
                                priority=priority, timer=timer, period=period)

    def move_task(self, source_id, destination_id):
        if self.cur_user is None:
            raise AttributeError('U R not logged in')
        self.cur_user.move_task(source_id, destination_id)

    def remove_task(self, task_id):
        if not self.cur_user:
            raise AttributeError('You are not logged in')
        self.cur_user.remove_task(task_id)

    def get_task_list(self, list_type='current'):
        if not self.cur_user:
            raise AttributeError('You are not logged in')
        s = None
        if list_type == 'current':
            s = self.cur_user.tasks.print_list()
        elif list_type == 'completed':
            s = self.cur_user.completed.print_list()
        elif list_type == 'failed':
            s = self.cur_user.failed.print_list()
        return s

    def load_board(self, board_id):
        folder = os.path.join(self.database_folder, self.boards_folder)
        if not os.path.exists(folder):
            pathlib.Path(folder).mkdir(parents=True, exist_ok=True)
            self.board = None
            raise FileNotFoundError('No board with current id')
        elif os.path.exists(os.path.join(folder, str(board_id) + '.json')):
            with open(os.path.join(folder, str(board_id) + '.json'), 'r') as f:
                try:
                    board = jsonpickle.decode(f.readline())
                    self.board = board
                except Exception as e:
                    self.board = None
                    raise e
        else:
            raise FileNotFoundError('No board with current id')

    def save_project(self):
        folder = os.path.join(self.database_folder, self.boards_folder)
        if not os.path.exists(folder):
            pathlib.Path(folder).mkdir(parents=True, exist_ok=True)
        if self.board:
            with open(os.path.join(folder, str(self.board.id) + '.json'), 'w+') as f1, \
                    open(os.path.join(folder, 'last_board.json'), 'w+') as f2:
                try:
                    f1.write(jsonpickle.encode(self.board))
                    f2.write(jsonpickle.encode(self.board))
                except Exception as e:
                    self.board = None
                    raise e

    def new_board(self, board_name):
        self.board = Board.Board(board_name)

    def add_board_task(self, name, description, tags, dependence=0, board_id=0,
                       priority=Priority.small, end=None, period=None):
        if not board_id and not self.board:
            raise KeyError('Project is not loaded')
        if board_id:
            self.load_board(board_id)
        if self.board:
            task = TaskBoard(name=name, description=description, tags=tags,
                             dependence=dependence, creator=self.cur_user.name,
                             priority=priority, end=end, period=period)
            self.board.add_task(task, self.cur_user.name)
            self.cur_user.boards.append(board_id)

    def edit_board_task(self, task_id, name, description, tags,
                        board_id=0, priority=Priority.small, end=None, period=None):
        if board_id:
            self.load_board(board_id)
        self.board.edit_task(task_id=task_id, name=name, description=description, tags=tags,
                             priority=priority, end=end, period=period)

    def get_boards(self):
        folder = os.path.join(self.database_folder, 'boards')
        boards = []
        if not os.path.exists(self.database_folder):
            os.mkdir(self.database_folder)
            return ''
        elif os.path.exists(folder):
            files = [f for f in os.listdir(folder) if os.path.isfile(os.path.join(folder, f))]
            for file_name in files:
                if file_name == 'last_board.json':
                    continue
                with open(os.path.join(folder, file_name), 'r') as f:
                    board = jsonpickle.decode(f.readline())
                    boards.append(str(board))
        return boards

    def get_board_task_list(self, list_type='current', board_id=0):
        if board_id:
            self.load_board(board_id)
        if not self.board:
            raise AttributeError('Cannot get task list: there is no loaded board.')
        if list_type == 'current':
            return self.board.pending_tasks.print_list()
        elif list_type == 'completed':
            return self.board.completed_tasks.print_list()
        elif list_type == 'failed':
            return self.board.failed_tasks.print_list()

    def complete_board_task(self, task_id, board_id=0):
        if board_id:
            self.load_board(board_id)
        if not self.board:
            raise AttributeError('Project is not loaded')
        self.board.complete_task(task_id, self.cur_user.name)

    def remove_board_task(self, task_id, board_id=0):
        if board_id:
            self.load_board(board_id)
        if not self.board:
            raise AttributeError('Project is not loaded')
        self.board.remove_task(task_id)

    def move_board_task(self, source_id, destination_id, project_id=0):
        if project_id:
            self.load_board(project_id)
        if not self.board:
            raise AttributeError('Project is not loaded')
        self.board.move_task(source_id, destination_id)

    def get_full_task_info(self, task_id):
        try:
            s = self.cur_user.get_full_task_info(task_id)
        except KeyError as e:
            raise e
        return s

    def get_full_board_task_info(self, task_id, board_id):
        try:
            if board_id:
                self.load_board(board_id)
            s = self.board.get_full_task_info(task_id)
        except KeyError as e:
            raise e
        return s

    def get_board_users(self, board_id=0):
        if board_id:
            self.load_board(board_id)
        if not self.board:
            raise AttributeError('Board is not loaded')
        users = []
        removed_users = []
        for user_id in self.board.users:
            if user_id in self.users:
                users.append(str(self.users[user_id]))
            else:
                removed_users.append(user_id)
        for user_id in removed_users:
            self.board.users.remove(user_id)
        return users

    def sort_user_tasks(self, sort_type):
        if not self.cur_user:
            raise AttributeError('You are not logged in')
        sorts = {
            'name': lambda task: task.name,
            'timer': lambda task: task.timer if task.timer else datetime(year=3000, month=12, day=12),
            'priority': lambda task: task.priority}
        if sort_type in sorts:
            self.cur_user.tasks.sort_by(sorts[sort_type])
        else:
            raise KeyError('No such sort type')

    def sort_board_tasks(self, sort_type, project_id=0):
        if project_id:
            self.load_board(project_id)
        if not self.board:
            raise AttributeError('Project is not loaded')
        sorts = {
            'name': lambda task: task.name,
            'timer': lambda task: task.timer if task.timer else datetime(year=3000, month=12, day=12),
            'priority': lambda task: task.priority
        }
        if sort_type in sorts:
            self.board.pending_tasks.sort_by(sorts[sort_type])
        else:
            raise KeyError('No such sort type')