from components.pars.dataparser import *


__all__ = ['get_path_for_users', 'get_auto_clear_messages',
           'get_type_notification']

CURRENT_USER_FILE_NAME = '.current'
CONFIG_NAME = os.path.join(os.path.expanduser('~'), '.settings.ini')


def create_config(path=None):
    config = configparser.ConfigParser()
    config.add_section('settings')
    config.set('settings', 'current_user', 'None')
    config.set('settings', 'path_for_users', 'None')
    config.set('settings', 'notification', '1 days|1 hours')
    config.set('settings', 'auto_clear_messages', 'true')
    if path is None:
        path = CONFIG_NAME
    else:
        path = os.path.join(path, CONFIG_NAME)
    with open(path, 'w') as config_file:
        config.write(config_file)


def load_config(path=None):
    config = configparser.ConfigParser()
    if path is None:
        path = CONFIG_NAME
    else:
        path = os.path.join(path, CONFIG_NAME)
    if not os.path.exists(path):
        create_config()
    config.read(path)
    return config


def get_path_for_users(path=None):
    config = load_config(path)
    path_for_users = config.get('settings', 'path_for_users')
    if path_for_users == 'None':
        path_for_users = os.path.join(os.getcwd(), 'users')
    return path_for_users


def set_current_user(path_to_all_users, user_name):
    full_path = os.path.join(path_to_all_users, CURRENT_USER_FILE_NAME)
    with open(full_path, 'w') as file:
        file.write(user_name)


def get_current_user():
    full_path = os.path.join(get_path_for_users(), CURRENT_USER_FILE_NAME)
    if os.path.exists(full_path):
        with open(full_path, 'r') as file:
            user_name = file.read()
        if user_name != '':
            return user_name
    else:
        with open(full_path, 'w'):
            pass


def get_auto_clear_messages(path=None):
    config = load_config(path)
    is_auto_clear_messages = config.get('settings', 'auto_clear_messages')
    if is_auto_clear_messages == 'true':
        return True
    return False


def get_type_notification(path=None):
    config = load_config(path)
    return config.get('settings', 'notification')
