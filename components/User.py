from components.TaskList import TaskList
from components.TaskInstructions import Priority, TypeOfStatus
import random


class User:
    def __init__(self, name='name', password='qwerty', messages=None):
        self.name = name
        self._password = password
        self.id = random.randint(0, 1000)
        self.tasks = TaskList({})
        self.completed = TaskList({})
        self.failed = TaskList({})
        self.boards = list()
        if messages is None:
            self.messages = messages
        else:
            self.messages = messages

    def leave_message(self, message):
        self.messages.append(message)

    def update_tasks(self):
        failed_tasks = self.tasks.get_failed()

        def _fail_task(task_id):
            task_to_fail = self.tasks.get_task(task_id)
            if not task_to_fail or task_to_fail.period:
                return
            task_to_fail.status = TypeOfStatus.failed
            self.failed.add_task(task_to_fail)
            tasks = task_to_fail.subtasks
            if not tasks:
                self.tasks.remove_task(task_id)
                return
            for item in tasks:
                _fail_task(item)
            self.tasks.remove_task(task_id)
        for task in failed_tasks:
            _fail_task(task)

    def check_password(self, password):
        return self._password == password

    def add_task(self, task):
        self.tasks.add_task(task)
        if task.dependencies:
            self.tasks.add_child(task.id, task.dependencies)

    def complete_task(self, task_id):
        task_to_complete = self.tasks.get_task(task_id)
        if not task_to_complete:
            return
        if task_to_complete.period:
            task_to_complete.timer += task_to_complete.period
            return
        self.completed.add_task(task_to_complete)
        self.tasks.complete_task(task_id)
        tasks = task_to_complete.subtasks
        if not tasks:
            return
        for task in tasks:
            self.complete_task(task)

    def edit_task(self, task_id, name=None, description=None, tags=None, priority=Priority.small, timer=None,
                  period=None):
        if task_id not in self.tasks.tasks:
            raise KeyError('Task with current id does not exist')
        self.tasks.tasks[task_id].change(name=name, description=description, tags=tags, priority=priority,
                                         timer=timer, period=period)

    def move_task(self, s_id, destination_id):
        if s_id not in self.tasks.tasks or destination_id not in self.tasks.tasks:
            raise KeyError('No task with current id')
        current_id = destination_id
        while current_id:
            current_id = self.tasks.tasks[current_id].dependencies
            if current_id == s_id:
                raise RecursionError('Destination task is one of the source task sub tasks')
        try:
            self.tasks.add_child(s_id, destination_id)
        except Exception as e:
            raise e

    def remove_task(self, task_id):
        if task_id in self.tasks.tasks:
            self.tasks.remove_task(task_id)
        elif task_id in self.completed.tasks:
            self.completed.remove_task(task_id)
        elif task_id in self.failed.tasks:
            self.failed.remove_task(task_id)

    def get_full_task_info(self, task_id):
        if task_id in self.tasks.tasks:
            return self.tasks.tasks[task_id].full_info()
        elif task_id in self.completed.tasks:
            return self.completed.tasks[task_id].full_info()
        elif task_id in self.failed.tasks:
            return self.failed.tasks[task_id].full_info()
        else:
            raise KeyError('Task with id: {id} does not exist'.format(id=task_id))

    def __str__(self):
        return self.name
