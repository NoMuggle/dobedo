from components.TaskInstructions import TypeOfStatus, Priority
from components.pars.dataparser import *
import hashlib


class Task:
    def __init__(self, name='task', tags=None, description='', priority=Priority.small,
                 dependence=0, end='', period=None, types_notification=''):

        self.start = datetime.now()
        self.timer = None
        if end is not None:
            self.timer = parse_date(end)
        self.changes = list()
        self.period = period
        if period is not None:
            if not end:
                raise NotImplementedError('Simple task cannot be repeating')
            self.period = get_next_date(period)
        self.name = name
        self.priority = priority
        self.description = description
        self.tag = []
        if tags and isinstance(tags, list):
            self.tag = tags
        self.dependencies = dependence
        self.subtasks = list()
        self.id = 0
        self.status = TypeOfStatus.current
        self.types_notification = types_notification
        self.id = hashlib.sha224(bytes(str(self), 'utf-8')).hexdigest()[:10]

    def period_is_complete(self):

        if not self.timer:
            return False
        if self.timer < datetime.now():
            if self.period:
                while self.timer < datetime.now():
                    self.timer += self.period
                return False
            self.status = TypeOfStatus.failed
            return True
        if self.period:
            return False
        return False

    def change(self, description=None, tags=None,
               name=None, priority=Priority.small, timer=None, period=None):
        if timer:
            self.timer = parse_date(timer)
        if period:
            self.period = get_next_date(period)
        if description:
            self.description = description
        if tags:
            self.tag = tags
        if name:
            self.name = name
        if not priority == Priority.small:
            self.priority = priority
        self.changes.append(datetime.now())

    def __str__(self):
        return 'id: {id} | {name} | Timer: {date} | Priority: {priority}| {status} '.format(
            id=self.id,
            name=self.name,
            status=self.status.name,
            date=self.timer,
            descr=self.description,
            priority=self.priority
        )

    def reset_dependency(self, sub_task_id):
        self.subtasks.remove(sub_task_id)

    def tasks_binding(self, id_task):
        if id_task not in self.subtasks:
            self.subtasks.append(id_task)

    def full_info(self):

        return """Task id: {id}
        Name: {name}
        Status: {status}
        Start: {start}
        Timer: {date}
        Priority: {priority}
        Description: {description}
        Dependence with id: {parent_id}
        Tags: {tags}
        Period: {period}
        Changed: {changed}
        Subtasks: {sub}""".format(
            id=self.id,
            name=self.name,
            status=self.status.name,
            date=self.timer,
            descr=self.description,
            priority=self.priority,
            description=self.description,
            parent_id=self.dependencies,
            tags=', '.join(self.tag),
            period=self.period,
            start=self.start,
            changed=', '.join(self.changes),
            sub=', '.join(self.subtasks)
        )

    def check_fail(self):
        if not self.timer:
            return False
        if self.timer < datetime.now():
            if self.period:
                while self.timer < datetime.now():
                    self.timer += self.period
                return False
            self.status = TypeOfStatus.failed
            return True
        if self.period:
            return False
        return False