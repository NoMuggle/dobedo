from enum import Enum


class Priority(Enum):
    small = 0
    medium = 1
    large = 2
    critical = 3


class TypeOfStatus(Enum):
    current = 0
    completed = 1
    failed = 2
