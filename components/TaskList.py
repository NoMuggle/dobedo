from components.TaskInstructions import TypeOfStatus


class TaskList:
    def __init__(self, tasks=None):
        # self.name = name
        if tasks:
            if isinstance(tasks, dict):
                self.tasks = tasks
            elif isinstance(tasks, list):
                self.tasks = {}
                for task in tasks:
                    self.tasks[task.id] = task
        else:
            self.tasks = {}
        self.root_tasks = [task_id for task_id in self.tasks if self.tasks[task_id].dependencies == 0]

    def add_task(self, task):
        self.tasks[task.id] = task
        if task.id not in self.root_tasks and task.dependencies == 0:
            self.root_tasks.append(task.id)

    def remove_task(self, task_id):
        if task_id not in self.tasks:
            return
        tasks = list(self.tasks[task_id].subtasks)
        for task in tasks:
            self.remove_task(task)
        del self.tasks[task_id]
        if task_id in self.root_tasks:
            self.root_tasks.remove(task_id)

    def complete_task(self, task_id):
        if self.tasks[task_id].period:
            self.tasks[task_id].timer += self.tasks[task_id].period
            return
        self.tasks[task_id].status = TypeOfStatus.completed
        del self.tasks[task_id]
        if task_id in self.root_tasks:
            self.root_tasks.remove(task_id)

    def search_task(self, tag):
        for task in self.tasks:
            if tag == task.tag:
                return task

    def get_task(self, task_id):
        return self.tasks.get(task_id)

    def get_failed(self):
        failed = []
        for task_id in self.tasks:
            if self.tasks[task_id].check_fail():
                failed.append(task_id)
        return failed

    def sort_by(self, rule):
        self.root_tasks.sort(key=lambda x: rule(self.tasks[x]))
        for key in self.tasks:
            task = self.tasks[key]
            task.subtasks.sort(key=lambda x: rule(self.tasks[x]))

    def add_child(self, task_id, parent_id):
        if task_id not in self.tasks:
            raise KeyError('task id: ' + task_id + ' does not exist')
        if parent_id and parent_id not in self.tasks:
            raise KeyError('task id: ' + parent_id + ' does not exist')

        if task_id in self.root_tasks:
            self.root_tasks.remove(task_id)
        self.tasks[task_id].dependencies = parent_id
        self.tasks[parent_id].tasks_binding(task_id)

    def print_list(self):
        used = {}
        s = []

        def _print(tasks, level):
            tasks = [self.tasks[key] for key in tasks if key in self.tasks]
            for task in tasks:
                if task and not used.get(task.id, False):
                    used[task.id] = True
                    s.append('|'+'____' * level + str(task))
                    _print(task.subtasks, level + 1)
        _print(self.root_tasks, 0)
        return s

    def __str__(self):
        inf = []
        for task in self.tasks:
            inf.append(inf(self.tasks[task]))
        return '|______'.join(inf)