from components.TaskList import *
from components.TaskInstructions import Priority
import hashlib


class Board:
    def __init__(self, name='Board'):
        self.name = name
        self.id = 0
        self.users = list()
        self.pending_tasks = TaskList({})
        self.completed_tasks = TaskList({})
        self.failed_tasks = TaskList({})
        self.id = hashlib.sha224(bytes(str(self), 'utf-8')).hexdigest()[:10]

    def add_user(self, name):
        self.users.append(name)

    def add_task(self, task, created_user_login):
        self.pending_tasks.add_task(task)
        if task.dependencies:
            self.pending_tasks.add_child(task.id, task.dependencies)
        if created_user_login not in self.users:
            self.add_user(created_user_login)
        task.created_user = created_user_login

    def edit_task(self, task_id, name=None, description=None, tags=None,
                  priority=Priority.small, end=None, period=None):
        if task_id not in self.pending_tasks.tasks:
            raise KeyError('Task with current id does not exist')
        self.pending_tasks.tasks[task_id].change(name=name, description=description, tags=tags, priority=priority,
                                                 timer=end, period=period)

    def get_full_task_info(self, task_id):
        if task_id in self.pending_tasks.tasks:
            return self.pending_tasks.tasks[task_id].full_info()
        elif task_id in self.completed_tasks.tasks:
            return self.completed_tasks.tasks[task_id].full_info()
        elif task_id in self.failed_tasks.tasks:
            return self.failed_tasks.tasks[task_id].full_info()
        else:
            raise KeyError('Task with id: {id} does not exist'.format(id=task_id))

    def complete_task(self, task_id, completed_user):
        task_to_complete = self.pending_tasks.get_task(task_id)
        if not task_to_complete:
            return
        if task_to_complete.period:
            task_to_complete.timer += task_to_complete.period
            return
        task_to_complete.completed_user = completed_user
        self.completed_tasks.add_task(task_to_complete)
        self.pending_tasks.complete_task(task_id)
        tasks = task_to_complete.subtasks
        if not tasks:
            return
        for task in tasks:
            self.complete_task(task, completed_user)

    def remove_task(self, task_id):
        if task_id in self.pending_tasks.tasks:
            self.pending_tasks.remove_task(task_id)
        elif task_id in self.completed_tasks.tasks:
            self.completed_tasks.remove_task(task_id)

    # def update_tasks(self):
    #     failed_tasks = self.pending_tasks.get_failed()
    #
    #     def _fail_task(task_id):
    #         task_to_fail = self.pending_tasks.get_task(task_id)
    #         task_to_fail.status = TypeOfStatus.failed
    #         self.failed_tasks.add_task(task_to_fail)
    #         try:
    #             tasks = task_to_fail.subtasks
    #         except Exception:
    #             self.pending_tasks.remove_task(task_id)
    #             return
    #         if not tasks:
    #             return
    #         for task in tasks:
    #             _fail_task(task)
    #         self.pending_tasks.remove_task(task_id)
    #     for task in failed_tasks:
    #         _fail_task(task)

    def move_task(self, source_id, destination_id):
        if source_id not in self.pending_tasks.tasks or destination_id not in self.pending_tasks.tasks:
            raise KeyError('No task with current id')
        cur_id = destination_id
        while cur_id:
            cur_id = self.pending_tasks.tasks[cur_id].dependencies
            if cur_id == source_id:
                raise RecursionError('Destination task is one of the source task sub tasks')
        try:
            self.pending_tasks.add_child(source_id, destination_id)
        except Exception as e:
            raise e

    def __str__(self):
        return 'Id: {id}\nName: {name}\nPending tasks amount: {pending}\nCompleted tasks amount: {completed}\n'.format(
            id=self.id,
            name=self.name,
            pending=len(self.pending_tasks.tasks),
            completed=len(self.completed_tasks.tasks)
        ) + 'Users: ' + ', '.join(self.users)