from components.TaskInstructions import Priority
from components.Task import Task


class TaskBoard(Task
                ):
    def __init__(self, name='Board_task', description='', dependence=0,
                 tags=None, creator=None, priority=Priority.small, end=None, period=None, types_notifications=''):
        self.creator = creator
        self.completed_user = None
        super().__init__(name, tags, priority, description, dependence, end, period, types_notifications)

    def __str__(self):
        return super().__str__() + ' | ' + self.creator + '\\' + str(self.completed_user)